package prime.com;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class DescompunereNumerePrime {
	
	private static List<Integer> listaNumerePrime = new ArrayList<Integer>();
	private static List<Integer> listaComponenteNumerePrime = new ArrayList<Integer>();
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		List<Integer> nrPrime=new ArrayList<Integer>();
		System.out.println("Introduceti limita minima:");
		int min = in.nextInt();
		System.out.println("Introduceti limita maxima:");
		int max = in.nextInt();
		nrPrime=PopuleazaPrime(max);
		int offset=CalculeazaOffset(min,max);
		offset=nrPrime.indexOf(offset);
		if(max>2)
		{
		for(int i=offset;i<nrPrime.size();i++)
		{
				Descompunere(nrPrime,nrPrime.get(i),offset);
		}
		}
		else
		{
			System.out.println("Valori intrare incorecte");
			System.exit(0);
		}
		
		//afisare val  
		int rezNrPrim=Collections.max(listaComponenteNumerePrime);
		int rezIndex=listaComponenteNumerePrime.lastIndexOf(rezNrPrim);
		System.out.println("Numarul cautat este:"+listaNumerePrime.get(rezIndex)+" si este format din:"+rezNrPrim+" numere prime consecutive");
		in.close();
	}
	
	//iau fiecare numar prim si il descompun
	public static void Descompunere(List<Integer> nrPrime,int nr,int offset)
	{
		int k=0;
		for(int j=0;j<offset+1;j++)
		{
			k=j;
		int sum=0;
		int contorListaPrime=k;//numere din lista care le adun la sum
		int contorEliminariLista=k;//numere din lista care le scad la sum
		int contor=0;
		while(sum<nr)
		{
			if(sum==nr)
			{
				break;
			}
			sum+=nrPrime.get(k);
			contorListaPrime++;
			k++;
			contor++;
			while(sum>nr)
			{
				if(sum==nr||(sum-nrPrime.get(contorEliminariLista))<nr)
				{
					break;
				}
				sum-=nrPrime.get(contorEliminariLista);
				contorEliminariLista++;
			}
		}
		if(sum==nr)
		{
		contor=contorListaPrime-contorEliminariLista;
		listaNumerePrime.add(nr);
		listaComponenteNumerePrime.add(contor);
		}
		}
		
	}
	//det toate nr prime pana la maxim
	public static List<Integer> PopuleazaPrime(int max)
	{
		List<Integer> nrPrime=new ArrayList<Integer>();
		int flagPrim=1;
		for(int i=1;i<=max;i++)
		{
			if(i==0||i==1)
				flagPrim=1;
			for(int j=2;j*j<=i;j++)
			{
				if(i%j==0)
					flagPrim=1;
			}
			if(flagPrim!=1)
			{
				nrPrime.add(i);
			}
			flagPrim=0;
		}
		return nrPrime;
	}
	
	public static int CalculeazaOffset(int min,int max)
	{
		int offset=2;
		int flagPrim=1;
		for(int i=min;i<=max;i++)
		{
			if(i==0||i==1)
				flagPrim=1;
			for(int j=2;j*j<=i;j++)
			{
				if(i%j==0)
					flagPrim=1;
			}
			if(flagPrim!=1)
			{
				offset=i;
				break;
			}
			flagPrim=0;
		}
		return offset;
	}
}
